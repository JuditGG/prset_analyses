#!/usr/bin/env nextflow

////////////////////////////////////////////////////////////////////
//
//  This script is responsible to perform the supervised subtyping
//  analyses. There are three different types of subtyping:
//  1. Composite cases vs controls, differentiate case vs control
//  2. Trait A vs Trait B, diffferentiate two traits
//  3. Trait A in Trait B, differentiate Trait B samples with and
//     without Trait A
//  We also compare the performance of PRSet against lassosum and
//  PRSice, investigate if performance gain by PRSet is due to 
//  GLM-net or true biological information (GTF shift)
////////////////////////////////////////////////////////////////////

nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.3'
timestamp='2021-04-01'
if(params.version) {
    System.out.println("")
    System.out.println("Real data segregation analysis - Version: $version ($timestamp)")
    exit 1
}
params.wind3 = 10
params.wind5 = 35
params.xregion = "chr6:25000000-34000000"

if(params.help){
    System.out.println("")
    System.out.println("Real data segregation analysis - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 4_real_segregation.nf [options]")
    System.out.println("Mandatory arguments:")
    System.out.println("    --bfile       UK biobank genotype file ")
    System.out.println("    --fam         QCed fam file ")
    System.out.println("    --snp         QCed SNP file ")
    System.out.println("    --out         Output prefix ")
    System.out.println("    --gmt         Folder containing the GMT files")
    System.out.println("    --gtf         GTF reference file")
    System.out.println("    --sql         UK biobank phenotype data base")
    System.out.println("    --lassosum    lassosum Rscript")
    System.out.println("    --json        JSON contain phenotype information")
    System.out.println("    --dropout     Drop out samples")
    System.out.println("    --cov         Covariate with 40 PCs and batch")
    System.out.println("Options:")
    System.out.println("    --wind3       Padding to 3' end ")
    System.out.println("    --wind5       Padding to 5' end")
    System.out.println("    --xregion     Region to be excluded from the analysis ")
    System.out.println("    --help        Display this help messages")
}

////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}


////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   combine_map
            addMeta
            removeMeta }   from './modules/handle_meta'
include {   extract_phenotype_from_sql
            process_phenotype_for_segregation
            assign_fold_to_composite_trait
            assign_fold_to_single_trait
            split_samples_for_cross_validation
            dichotomize_phenotype }   from './modules/phenotype_extraction'
include {   meta_analysis
            modify_metal
            standardize_summary_statistic
            filter_summary_statistic  }   from './modules/basic_genetic_analyses'
include {   prset_analysis
            prset_analysis as highResPRSet
            extract_significant_gmt
            prsice_analysis
            prsice_analysis as allScorePRSice
            lassosum_analysis   }   from './modules/polygenic_score'
include {   supervised_pathway_classification
            supervised_classification_with_single_prs
            unsupervised_classification_with_set
            unsupervised_classification_with_single_prs   }   from './modules/classification'
include {   gene_shift_gtf
            filter_gmt
            combine_files
            combine_files as combine_sets
            combine_files as combine_unsupervised  }   from './modules/misc'
////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////


// Trying to parse the JSON input
import groovy.json.JsonSlurper
def jsonSlurper = new JsonSlurper()
// new File object from your JSON file
def ConfigFile = new File("${params.json}")
// load the text from the JSON
String ConfigJSON = ConfigFile.text
// create a dictionary object from the JSON text
def phenoConfig = jsonSlurper.parseText(ConfigJSON)

// Set based GMT files. The map groupTulpe map combo is so that 
// I can use path("*") in the process to capture all GMT files
// in one go
gmt = Channel.fromPath("${params.gmt}/*.gmt") 

// GTF file, for PRSet
gtf = Channel.fromPath("${params.gtf}")

// UK Biobank genotype data. Will check if the file exists
genotype = Channel.fromFilePairs("${params.bfile}.{bed,bim,fam}", size:3 , flat: true){ file -> file.baseName } \
    | ifEmpty{ error "No matching plink files "} \
    | map{ a -> [ fileExists(a[1]), fileExists(a[2]), fileExists(a[3])]}

// UK Biobank SQL file. This is where we extract the phenotype from
sql = Channel.fromPath("${params.sql}")

// UK Biobank covariate file 
cov = Channel.fromPath("${params.cov}")

// QC and drop out information. Use to select samples and SNPs
dropout = Channel.fromPath("${params.dropout}")
qcFam = Channel.fromPath("${params.fam}")
snp = Channel.fromPath("${params.snp}")

// Parameter for set based analysis. Default is to remove the MHC region
// and extend each gene region 35kb to 5' and 10kb to 3'
wind3 = Channel.of("${params.wind3}")
wind5 = Channel.of("${params.wind5}")

// Define the cross validation fold
cv_fold = Channel.of(1..5) \
    | flatten
maxFold = cv_fold.max()
// Software path
lassosum = Channel.fromPath("${params.lassosum}")

// Parse the summary statistic meta data
// This script extract information from the JSON file and organize it 
// in format that is recognized by downstream porcesses
sumstat = Channel.from(phenoConfig.collect{ content ->
    [   [   name: content.name,         // organize this into map
            phenoName: content.name], 
        file(content.gwas), 
        content.rsid,
        content.a1,
        content.a2,
        content.statistic,
        content.pvalue,
        content.se,
        content.ncol,
        content.sampleSize,
        content.isBeta
    ]})

// Parse the xregions from the summary statistic meta data
xregions = Channel.from(phenoConfig.collect{ content ->
    [   [   name: content.name,         // organize this into map
            phenoName: content.name], 
        content.xregion
    ]})

// Parse the phenotype meta data
// Here, we use the GWAS as stub, 
// we won't use it at all, but this allow us to reuse processes from provious analysis
pheno = Channel.from(phenoConfig.collect{ content ->
    [   [   name: content.name,         // organize this into map
            phenoName: content.name], 
        file(content.phenoFile),
        file(content.gwas)   
    ]}) 
    

workflow{
    // 1. Prepare the phenotype. We use the pseudo residuals as the 
    //    phenotype for our downstream analysis as using the full 
    //    covariate matrix will be too time consuming
    phenotype_preparation()
    // 2. Modify the summary statistics. Mainly want to meta-analyzed
    //    GWAS of two disease and generate the "composite" GWAS. This is
    //    not ideal as what we are doing here violates the underlying assumption
    //    of meta analysis. As a result of that, instead of using inverse variant
    //    meta-analysis, we use the p-value analysis with sample size as weight
    sumstat_preparation()
    // 3. The main polygenic score analysis. We will perform PRSet, PRSice and lassosum 
    //    on our data. There are 3 type of analyses here:
    //    1. PRSice / lassosum 
    //       1. Run PRSice or lassosum on training data set 
    //       2. Extract PRS with "best" prediction in training data
    //       3. Use the same parameter, estimate prediction using validation data
    //    2. PRSet
    //       1. Run PRSet on training data set with 10,000 permutation to obtain competitive
    //          p-value
    //       2. Select any pathways with competitive p-value < 0.05
    //       3. Use cv.glmnet with PRS of pathway selected in 2 to perform lasso and obtain "best"
    //          performing model in training dataset
    //       4. Apply the "best" glmnet model in the validation data to obtain the R2
    //    3. PRSetHybrid
    //       1. Run PRSet on training data set with 10,000 permutation to obtain competitive
    //          p-value
    //       2. Select any pathways with competitive p-value < 0.05
    //       3. Re-run PRSet on pathway selected in 2 on training data. This time, enable p-value
    //          thresholding. For each set, the p-value threshold that provide best prediction of
    //          phenotype in training data set is selected
    //       4. Use PRS from "best" p-value threshold for each gene set as input to cv.glmnet.
    //          Find best lasso parameter using training data.
    //       5. Apply the same p-value threshold and glmnet model in validation data to obtain 
    //          performance

    polygenic_risk_score_analysis(
        phenotype_preparation.out,
        sumstat_preparation.out
    )
}

workflow phenotype_preparation{
    // 1. Extract Extreme Height (top 5% Height in each sex), Obesity ( BMI > 30), CAD and T2D from 
    //    the SQL data base
    pheno \
        | combine(sql) \
        | extract_phenotype_from_sql
    // For LDL and Height, as they are continuous phenotypes, we would like to convert them 
    // into binary phenotypes. (top 5% of height in each sex, LDLc > 4.9 after adjusting for
    // Statin usage)
    continuous = extract_phenotype_from_sql.out \
        | map{ a -> [   a[0],   // meta information
                        a[2]]}  /* phenotype */\
        | filter{ a -> a[0].name == "LDL" || a[0].name == "Height"} \
        | dichotomize_phenotype
    /*
        We can then mix the dichotomized LDL and height data phenotype
        with the other binary phenotypes to form the phenotype group needed
        for downstream analysis
    */
    allPhenos = extract_phenotype_from_sql.out \
        | map{ a -> [   a[0],   // meta information  
                        a[2]]}  /* phenotype file */\
        | filter{ a -> a[0].name != "LDL" && a[0].name != "Height"} \
        | mix(continuous)
    // we want a combination of each phenotype
    // BMI + CAD, BMI + Height, BMI + T2D, CAD + Height, CAD + T2D, Height + T2D
    // Add the equality comparison so that we don't do Height vs Obesity and Obesity vs Height
    // can add CaseOnly if we figure out how to handle comorbid samples
    // 2. Generate the composite phenotype and assign cross-validation fold to each of them
    allPhenos \
        | combine(allPhenos) \
        /*  remove self to self */
        | filter(a -> a[0] != a[2]) \
        | combine(maxFold) \
        /*
            Here, we are trying to do three different type of analyses:
            All - Comparing Control (no disease) vs Cases (Either one of disease)
            Distinct - Comparing Samples with Trait 1 vs Samples with Trait 2 (no comorbid)
            Subtype - Comparing Samples with Trait 1 who also have Trait 2 against sample
            with Trait 1 but not Trait 2
            
            NOTE: This is the typeAscertain
        */
        | combine(Channel.of("All", "Distinct", "Subtype")) \
        /*
            Paul was worried about overfitting with our cross-validation stratigy. So we also do
            a sensitivity analysis where we split samples in half. This reduce our target sample
            and thus should "reduce" problem of overfitting

            NOTE: This is the typeAnalysis
        */
        | combine(Channel.of("CV", "Sensitivity")) \
        | map{  a ->    def meta = a[0].clone()
                        meta.name2 = a[2].name
                        meta.phenoName2 = a[2].phenoName
                        meta.typeAscertain = a[5]
                        meta.typeAnalysis = a[6]
                        if(meta.typeAscertain == "Subtype"){
                            meta.meta = false
                        }else{
                            meta.meta = true
                        }
                        return([
                            meta,   // meta information
                            a[1],   // traitA phenotype file
                            a[3],   // traitB phenotype file
                            a[4]    // max fold
                        ])} \
        /* 
            remove duplicated analysis where we have A -> B and B -> A with the exception of 
            when we are running the subtyping analysis

            First filter name < name2 means taht we will only do CAD T2D, but not T2D CAD
            Second filter will only be true if we are doing Subtyping
        */
        | filter( a -> a[0].name < a[0].name2 || a[0].typeAscertain == "Subtype") \
        | combine(cov) \
        | combine(qcFam) \
        | combine(dropout) \
        /*
            This function will generate the required split of samples, and assign the 
            fold to each sample without generating all the required files. The result
            file can then be splitted downstream
        */
        | assign_fold_to_composite_trait 

    // 3. Also generate single trait data to test whether glmnet is the reason of 
    //    PRSet's performance
    dicohtomized = Channel.of("T")
    /*
        Single trait analysis is added as a reference point. This allow us to see
        if the glmnet in downstream analysis are the reason for improved performance. 
    */
    allPhenos \
        | combine(Channel.of("CV", "Sensitivity")) \
        | map{  a ->    def meta = a[0].clone()
                        meta.typeAscertain = "Single"
                        meta.typeAnalysis = a[2]
                        meta.name2 = meta.name
                        meta.phenoName2 = meta.phenoName
                        meta.meta = false
                        return([
                            meta,   // meta information
                            a[1],   // traitA phenotype file
                        ])} \
        | combine(maxFold) \
        | combine(cov) \
        | combine(qcFam) \
        | combine(dropout) \
        /*
            Honestly can't remember why I need to provide dicohtomized = T here. 
            Maybe that is for another workflow
        */
        | combine(dicohtomized) \
        | assign_fold_to_single_trait
    // 4. Extract samples for each cross-validation fold. For sensititivy analysis,
    //    only do one fold
    assign_fold_to_composite_trait.out.pheno  \
        | mix(assign_fold_to_single_trait.out.pheno) \
        | combine(cv_fold) \
        /*
            Only perform the split if we are doing sensitivity analysis with cv of 1, or we are not doing
            sensitivity analysis
        */
        | filter{ a -> (a[0].typeAnalysis == "Sensitivity" && a[2] == 1) || (a[0].typeAnalysis != "Sensitivity")} \
        | split_samples_for_cross_validation
    // 5. Gather sample size information for ascertainment adjustment when plotting
    information = assign_fold_to_composite_trait.out.info \
        | mix(assign_fold_to_single_trait.out.info) \
        | collect 
    combine_files(  Channel.of("phenotype.info"),
                    Channel.of("result/phenotype"),
                    information)
    emit:
        split_samples_for_cross_validation.out
}

workflow sumstat_preparation{
    // need to prepare the summary statistic. The way Judit does is to first
    // perform a meta analysis
    /*
        To make downstream analysis easier, we first standardize the summary
        statistic in a format that is recognizable by all software (mainly 
        lassosum) and so that we can "hardcode" the column names without any
        troubles.
    */
    sumstat \
        | standardize_summary_statistic \
        | combine(snp) \
        | combine(genotype) \
        | combine(xregions, by: 0) \
        /* Do not remove ambiguous variants. Can't remember the reason why*/
        | combine(Channel.of("F")) \
        /*
            We want to perform pre-filtering of the xregion here as lassosum
            cannot do that. By performing the filtering here, we don't need to
            think about it downstream
        */
        | filter_summary_statistic
    /*
        We can combine teh summary statistics, together, to generate
        the required meta-analyzed summary statistics
    */
    filter_summary_statistic.out.sumstat \
        | combine(filter_summary_statistic.out.sumstat) \
        /* Don't allow self to self */
        | filter( a -> a[0] != a[2]) \
        /* Remove redundent analysis (CAD and T2D == T2D and CAD) */
        | filter( a -> a[0].name < a[2].name) \
        | map{  a ->    def meta = a[0].clone()
                        meta.name2 = a[2].name
                        meta.phenoName2 = a[2].phenoName
                        meta.meta = true
                        return(
                            [   meta,
                                a[1],
                                a[3]]
                        )} \
        | meta_analysis \
        | modify_metal
    /*
        In additional to the meta-analysis, we also want the single 
        GWAS for things like the subtyping analysis. We can achieve it
        by providing the same name name2 phenoName and phenoName2 in the
        meta storage
    */
    allPheno = filter_summary_statistic.out.sumstat \
        | map{ a -> [a[0].name,
                    a[0].phenoName] }
    gwas = filter_summary_statistic.out.sumstat \
        | combine(allPheno) \
        | map{ a -> def meta = a[0].clone()
                    meta.name2 = a[2]
                    meta.phenoName2 = a[3]
                    meta.meta = false
                    return(
                        [   meta,   // meta information
                            a[1]    // sumstat
                        ]
                    )} \
        | mix(modify_metal.out)
    emit:
        gwas
}

workflow polygenic_risk_score_analysis{
    take: pheno
    take: sumstat
    main:
        /*
            Previous PRSet analysis involve generating the Null MalaCards score
            gene sets which is not required here. So we will do the filtering and 
            this will also merge teh GMT into a single files.
        */
        filter_gmt(gmt.collect())
        // 1. Generate frame shifted GTF file as Null
        //    If the signals resides within the genic region, this
        //    shift will allow us to test whether the performance of 
        //    PRSet is due to the genic structure (no change in performance)
        //    or were driven by true signal (significantly decreased performance)
        //    By shifting 5mb, we lost around 7% of genes, so if the decrease is 
        //    less than 7%, then it might be problematic
        gtfNorm = Channel.of("Normal") \
            | combine(gtf)
        gene_shift_gtf(gtf, "5000000")
        gtfInput = gtfNorm \
            | mix(gene_shift_gtf.out)
        // 2. Perform prset analyses on the data
        /*
            We combine the phenotype and sumstat information based on their names
        */

        baseTarget = combine_map(x: pheno, y: sumstat, by:0, column: ["name", "name2", "meta"], dup: "remove") \
            | combine(snp) \
        /*
            This will generate two gtf input type, Normal (without frameshift) and Null (with frameshift)
        */
            | combine(gtfInput) \
            | combine(genotype) \
            | map{ a -> def meta = a[0].clone()
                        meta["gtf"] = a[5]
                        /*
                            We want to use the best score, so use true here
                        */
                        meta["keepBest"] = "true"
                        /*
                            Do not allow high-resolution scoring when 
                            performing enrichment analysis as that will 
                            cause problem in interpretation
                        */
                        meta["highRes"] = "false"
                        return([
                            meta,   // meta information
                            a[1],   // target phenotype
                            a[4],   // QCed SNPs
                            a[3],   // summary statistic
                            a[7],  // bed
                            a[8],  // bim
                            a[9],  // fam
                            a[6]   /*gtf*/ ])} 
        baseTarget \
            | combine(wind5) \
            | combine(wind3) \
            /*  
                Want to perform 10000 permutation to obtain the competitive p-value 
            */
            | combine(Channel.of(10000)) \
            | combine(filter_gmt.out) \
            | prset_analysis
        // 3. For each gene set that has competitive p-value < 0.05, 
        //    we will do high resolution scoring on them to do get 
        //    optimum signal
        prset_analysis.out.summary \
            | combine(filter_gmt.out) \
            /* 
                extract significant gmt will check for gene sets that has a competitive
                p less than 0.05 and extract them from the gmt files, generating a new 
                input
            */
            | extract_significant_gmt
            
        baseTarget \
            | combine(extract_significant_gmt.out, by: 0) \
            | map{ a -> def meta = a[0].clone()
                        /*
                            Because we wrote the process to use 
                            name as the output file prefix, using the
                            name directly here will cause a name collidsion
                            with previous prset run. As a result, we need to change
                            the name to name-hybrid. In order to be able to revert
                            this change, we also store the original name in the
                            oriName field in the meta information
                        */
                        meta["oriName"] = meta["name"]
                        meta["name"] = meta["name"]+"-hybrid"
                        /*
                            Set highRes to true so that we can 
                            get optimal signals
                        */
                        meta["highRes"] = "true"
                        return([
                            meta,   // meta information
                            a[1],   // target phenotype
                            a[2],   // QCed SNP
                            a[3],   // Sumstat
                            a[4],   // bed
                            a[5],   // bim
                            a[6],   // fam
                            a[7],   // gtf
                            "${params.wind5}",   // wind5
                            "${params.wind3}",  // wind3
                            0,      // Perm
                            a[8]    // GMT
                        ])
                        } \
            | highResPRSet
        /*
            After running the high-resolution PRSet, we will need to reformat
            the meta information so that we can merge that with the other PRSet 
            runs
        */
        highRes = highResPRSet.out.summary \
            | combine(highResPRSet.out.best, by: 0) \
            | combine(highResPRSet.out.snp, by: 0) \
            | map{ a -> def meta = a[0].clone()
                        meta.remove("name")
                        meta["name"] = meta.remove("oriName")
                        meta.remove("keepBest")
                        meta.remove("highRes")
                        return([
                            meta,
                            a[1],   // summary
                            a[2],   // best
                            a[3]    /* SNPs */ ])} \
            | combine(Channel.of("PRSet-hybrid"))

         prsRes = prset_analysis.out.summary \
            | combine(prset_analysis.out.best, by: 0) \
            | combine(prset_analysis.out.snp, by: 0) \
            | map{ a -> removeMeta(x: a, keys: ["keepBest", "highRes"])} \
            | combine(Channel.of("PRSet")) \
            | mix(highRes) 
        /*
            Combining the phenotype and PRSet results based on the type of ascertainment, analysis and folds for the
            specific traits in order to run the supervised pathway classificiation
        */
        combine_map(x: pheno, y: prsRes, by: 0, column: ["name", "name2", "meta", "typeAscertain", "fold", "typeAnalysis"]) \
            | supervised_pathway_classification
        /*
            Simularly, do the same for unsupervised classification. But remove data where we have the 
            same traits as we can't do the unsupervised classficiation on them (as of now)
        */
        combine_map(x: pheno, y: prsRes, by: 0, column: ["name", "name2", "meta", "typeAscertain", "fold", "typeAnalysis"]) \
            | filter{a -> a[0].name != a[0].name2} \
            | unsupervised_classification_with_set
        // 5. Perform PRSice analysis using both high resolution scoring and fast score
        //    We did fast score so that we don't get like ~7000 scores from PRSice, which 
        //    can be extremely memory intensive
        singlePRSInput = combine_map(x: pheno, y: sumstat, by:0, column: ["name", "name2", "meta"], dup: "remove") \
            | combine(snp) \
            | combine(genotype) \
            | map{ a -> def meta = a[0].clone()
                        return([
                            meta,   // meta information
                            a[1],   // target phenotype
                            a[4],   // QCed SNPs
                            a[3],   // summary statistic
                            a[5],   // bed
                            a[6],   // bim
                            a[7]    /* fam */])} 
        // run PRSice
        singlePRSInput \
            | map{ a -> addMeta(x: a, meta: ["allScore": "false"])} \
            | prsice_analysis
        // We also run a PRSice run where we generated all scores, we will use that
        // for our GLMNet model to see if PRSet's performance gain is due to the glmnet or 
        // the additional biochemical information we got
        singlePRSInput \
            | map{ a -> addMeta(x: a, meta: ["allScore": "true"])} \
            | allScorePRSice
        // Reformat the output in preparation of merging with the lassosum results 
        // This allow us to reuse processes 
        allScore = allScorePRSice.out.all  \
            | map{ a -> removeMeta(x: a, keys: ["allScore"])}
        prsiceRes = prsice_analysis.out.best \
            | map{ a -> removeMeta(x: a, keys: ["allScore"])} \
            | combine(allScore, by: 0) \
            | combine(Channel.of("PRSice"))
        // 6. Also perform PRS analysis using lassosum. This provide a rough comparison
        //    of PRSet against the state of the art software (not going to do LDpred2 as
        //    that is way too time consuming
        // our container has lassosum installed. but that doesn't include our
        // custome script that ue
        singlePRSInput \
            | map{ a -> addMeta(x: a, meta: ["allScore": "true", "best": "true"])} \
            | combine(lassosum) \
            | lassosum_analysis
        singlePRS = lassosum_analysis.out.best \
            | combine(lassosum_analysis.out.all, by: 0) \
            | map{ a -> removeMeta(x: a, keys: ["allScore", "best"])} \
            | combine(Channel.of("lassosum")) \
            | mix(prsiceRes)
        combine_map(x: pheno, y: singlePRS, by: 0, 
                column: ["name", "name2", "meta", "typeAscertain", "fold", "typeAnalysis"]) \
            | supervised_classification_with_single_prs
        
        // Cannot do unsupervised classification for the same phenotype
        combine_map(x: pheno, y: singlePRS, by: 0, 
                column: ["name", "name2", "meta", "typeAscertain", "fold", "typeAnalysis"]) \
            | filter{ a -> a[0].name != a[0].name2} \
            | unsupervised_classification_with_single_prs
        result = supervised_classification_with_single_prs.out \
            | mix(supervised_pathway_classification.out.res) \
            | collect
        combine_files("${params.out}-supervised.csv", "result", result)
        set_results = supervised_pathway_classification.out.set \
            | collect 
        combine_sets("${params.out}-sets.csv", "result", set_results)
        unsupervised = unsupervised_classification_with_single_prs.out \
                | mix(unsupervised_classification_with_set.out)\
                | collect
        combine_unsupervised("${params.out}-unsupervised.csv", "result", unsupervised)
}
