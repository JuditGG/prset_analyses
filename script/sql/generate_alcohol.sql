.mode csv
.header on 
.output Alcohol.csv
SELECT  s.sample_id as FID,  
        s.sample_id as IID,
        age.pheno as Age,
        sex.pheno as Sex,
        centre.pheno as Centre,
        (   COALESCE(q1.pheno,0)+
            COALESCE(q2.pheno,0)+
            COALESCE(q3.pheno,0)+
            COALESCE(q4.pheno,0)+
            COALESCE(q5.pheno,0)+
            COALESCE(q6.pheno,0)+
            COALESCE(q7.pheno,0)+
            COALESCE(q8.pheno,0)+
            COALESCE(q9.pheno,0)+
            COALESCE(q10.pheno,0)) as AUDIT_Total,
        (   COALESCE(q1.pheno,0)+
            COALESCE(q2.pheno,0)+
            COALESCE(q3.pheno,0)) as AUDIT_C,
        (   COALESCE(q4.pheno,0)+
            COALESCE(q5.pheno,0)+
            COALESCE(q6.pheno,0)+
            COALESCE(q7.pheno,0)+
            COALESCE(q8.pheno,0)+
            COALESCE(q9.pheno,0)+
            COALESCE(q10.pheno,0)) as AUDIT_P
FROM    Participant s 
        LEFT JOIN f20414 q1 ON
            s.sample_id=q1.sample_id 
            AND q1.Instance=0 
            AND q1.Pheno >=0
        LEFT JOIN f20403 q2 ON
            s.sample_id=q2.sample_id 
            AND q2.Instance=0 
            AND q2.Pheno >=0
        LEFT JOIN f20416 q3 ON
            s.sample_id=q3.sample_id 
            AND q3.Instance=0 
            AND q3.Pheno >=0
        LEFT JOIN f20413 q4 ON
            s.sample_id=q4.sample_id 
            AND q4.Instance=0 
            AND q4.Pheno >=0
        LEFT JOIN f20407 q5 ON
            s.sample_id=q5.sample_id 
            AND q5.Instance=0 
            AND q5.Pheno >=0
        LEFT JOIN f20412 q6 ON
            s.sample_id=q6.sample_id 
            AND q6.Instance=0 
            AND q6.Pheno >=0
        LEFT JOIN f20409 q7 ON
            s.sample_id=q7.sample_id 
            AND q7.Instance=0 
            AND q7.Pheno >=0
        LEFT JOIN f20408 q8 ON
            s.sample_id=q8.sample_id 
            AND q8.Instance=0 
            AND q8.Pheno >=0
        LEFT JOIN f20411 q9 ON
            s.sample_id=q9.sample_id 
            AND q9.Instance=0 
            AND q9.Pheno >=0
        LEFT JOIN f20405 q10 ON
            s.sample_id=q10.sample_id 
            AND q10.Instance=0 
            AND q10.Pheno >=0
        LEFT JOIN f31 sex ON
            s.sample_id=sex.sample_id 
            AND sex.instance = 0
        LEFT JOIN f21003 age ON 
            s.sample_id=age.sample_id 
            AND age.instance = 0
        LEFT JOIN f54 centre ON 
            s.sample_id=centre.sample_id 
            AND centre.instance = 0
WHERE   (
            q1.pheno IS NOT NULL
            OR q2.pheno IS NOT NULL
            OR q3.pheno IS NOT NULL
        )
        AND (
            q4.pheno IS NOT NULL
            OR q5.pheno IS NOT NULL
            OR q6.pheno IS NOT NULL
            OR q7.pheno IS NOT NULL
            OR q8.pheno IS NOT NULL
            OR q9.pheno IS NOT NULL
            OR q10.pheno IS NOT NULL
        );
.quit


