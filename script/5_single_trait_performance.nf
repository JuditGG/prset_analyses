#!/usr/bin/env nextflow

////////////////////////////////////////////////////////////////////
//
//  This script is almost identical to 4_real_segregation except
//  that we are using quantitative traits and only consider
//  single trait analyses
//
////////////////////////////////////////////////////////////////////

nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.1'
timestamp='2021-05-11'
if(params.version) {
    System.out.println("")
    System.out.println("Run single trait performance comparison  - Version: $version ($timestamp)")
    exit 1
}

params.wind3 = 10
params.wind5 = 35
if(params.help){
    System.out.println("")
    System.out.println("Run single trait performance comparison - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 7_single_trait_performance.nf [options]")
	System.out.println("Mandatory arguments:")
    System.out.println("    --bfile       UK biobank genotype file ")
    System.out.println("    --fam         QCed fam file ")
    System.out.println("    --snp         QCed SNP file ")
    System.out.println("    --out         Output prefix ")
    System.out.println("    --gmt         Folder containing the GMT files")
    System.out.println("    --gtf         GTF reference file")
    System.out.println("    --sql         UK biobank phenotype data base")
    System.out.println("    --prsice      PRSice executable")
    System.out.println("    --lassosum    lassosum Rscript")
    System.out.println("    --json        JSON contain phenotype information")
    System.out.println("    --dropout     Drop out samples")
    System.out.println("    --cov         Covariate with 40 PCs and batch")
    System.out.println("Options:")
    System.out.println("    --wind3       Padding to 3' end ")
    System.out.println("    --wind5       Padding to 5' end")
    System.out.println("    --help        Display this help messages")
} 


////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def gen_file(a, bgen){
    return bgen.replaceAll("@",a.toString())
}

def get_chr(a, input){
    if(input.contains("@")){
        return a
    }
    else {
        return 0
    }
}

////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   combine_map 
            addMeta
            removeMeta  }   from './modules/handle_meta'
include {   standardize_summary_statistic
            filter_summary_statistic    }   from './modules/basic_genetic_analyses'
include {   extract_phenotype_from_sql
            residualize_phenotypes
            assign_fold_to_single_trait
            split_samples_for_cross_validation  }   from './modules/phenotype_extraction'
include {   gene_shift_gtf
            filter_gmt
            combine_files
            combine_files as combine_sets }   from './modules/misc'
include {   prset_analysis
            prset_analysis as highResPRSet
            extract_significant_gmt
            prsice_analysis
            prsice_analysis as allScorePRSice
            lassosum_analysis  }   from './modules/polygenic_score'
include {   supervised_pathway_classification
            supervised_classification_with_single_prs  }   from './modules/classification'
////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////


// Trying to parse the JSON input
import groovy.json.JsonSlurper
def jsonSlurper = new JsonSlurper()
// new File object from your JSON file
def ConfigFile = new File("${params.json}")
// load the text from the JSON
String ConfigJSON = ConfigFile.text
// create a dictionary object from the JSON text
def phenoConfig = jsonSlurper.parseText(ConfigJSON)

// Set based GMT files. The map groupTulpe map combo is so that 
// I can use path("*") in the process to capture all GMT files
// in one go
gmt = Channel.fromPath("${params.gmt}/*.gmt")

// GTF file, for PRSet
gtf = Channel.fromPath("${params.gtf}")

// UK Biobank genotype data. Will check if the file exists
genotype = Channel.fromFilePairs("${params.bfile}.{bed,bim,fam}", size:3 , flat: true){ file -> file.baseName } \
    | ifEmpty{ error "No matching plink files "} \
    | map{ a -> [ fileExists(a[1]), fileExists(a[2]), fileExists(a[3])]}

// UK Biobank SQL file. This is where we extract the phenotype from
sql = Channel.fromPath("${params.sql}")

// UK Biobank covariate file 
cov = Channel.fromPath("${params.cov}")

// QC and drop out information. Use to select samples and SNPs
dropout = Channel.fromPath("${params.dropout}")
qcFam = Channel.fromPath("${params.fam}")
snp = Channel.fromPath("${params.snp}")

// Parameter for set based analysis. Default is to remove the MHC region
// and extend each gene region 35kb to 5' and 10kb to 3'
wind3 = Channel.of("${params.wind3}")
wind5 = Channel.of("${params.wind5}")
// Define the cross validation fold
cv_fold = Channel.of(1..5) \
    | flatten
maxFold = cv_fold.max()

// Software path
lassosum = Channel.fromPath("${params.lassosum}")

// Parse the summary statistic meta data
sumstat = Channel.from(phenoConfig.collect{ content ->
    [   [   name: content.name,         // organize this into map
            phenoName: content.name], 
        file(content.gwas), 
        content.rsid,
        content.a1,
        content.a2,
        content.statistic,
        content.pvalue,
        content.se,
        content.ncol,
        content.sampleSize,
        content.isBeta
    ]})


xregions = Channel.from(phenoConfig.collect{ content ->
    [   [   name: content.name,         // organize this into map
            phenoName: content.name], 
        content.xregion
    ]})


pheno = Channel.from(phenoConfig.collect{ content ->
    [   [   name: content.name,         // organize this into map
            phenoName: content.name], 
        file(content.phenoFile), 
        file(content.rscript)
    ]})

workflow{
    // 1. Prepare the phenotype. We use the pseudo residuals as the 
    //    phenotype for our downstream analysis as using the full 
    //    covariate matrix will be too time consuming
    phenotype_preparation()
    // 2. Modify the summary statistics to have a unified input
    sumstat_preparation()  
    // 3. Run polygenic risk score analysis
    
    polygenic_risk_score_analysis(
        phenotype_preparation.out,
        sumstat_preparation.out
    )
}


workflow sumstat_preparation{
    // prepare summary statistics so that they have identical formats
    sumstat \
        | standardize_summary_statistic \
        | combine(snp) \
        | combine(genotype) \
        | combine(xregions, by: 0) \
        | combine(Channel.of("F")) \
        | filter_summary_statistic
    gwas = filter_summary_statistic.out.sumstat \
        | map{ a -> return(addMeta(x: a, meta: ["name2": a[0].name]))}
    emit:
        gwas
}

workflow phenotype_preparation{
    // 1. Extract Phenotype from SQL 
    // 2. Residualize phenotype
    phenotype =  pheno \
        | combine(sql) \
        | extract_phenotype_from_sql \
        | combine(qcFam) \
        | combine(dropout) \
        | combine(cov) \
        | residualize_phenotypes \
        | map{  a ->    def meta = a[0].clone()
                        meta.typeAscertain = "Single"
                        meta.typeAnalysis = "CV"
                        meta.processed = "T"
                        meta.name2 = meta.name
                        meta.phenoName = meta.name
                        return([
                            meta,   // meta information
                            a[1],   // traitA phenotype file
                        ])} \
        | combine(maxFold) \
        | combine(cov) \
        | combine(qcFam) \
        | combine(dropout) \
        | combine(Channel.of("F")) \
        | assign_fold_to_single_trait
    assign_fold_to_single_trait.out.pheno \
        | map{ a -> removeMeta(x: a, keys: ["processed"])} \
        | combine(cv_fold) \
        | split_samples_for_cross_validation
    emit:
       split_samples_for_cross_validation.out
}


workflow polygenic_risk_score_analysis{
    take: pheno
    take: sumstat
    main:
        filter_gmt(gmt.collect())
        // 1. Generate frame shifted GTF file as Null
        //    If the signals resides within the genic region, this
        //    shift will allow us to test whether the performance of 
        //    PRSet is due to the genic structure (no change in performance)
        //    or were driven by true signal (significantly decreased performance)
        gtfNorm = Channel.of("Normal") \
            | combine(gtf)
        gene_shift_gtf(gtf, "5000000")
        gtfInput = gtfNorm \
            | mix(gene_shift_gtf.out)
        // 2. Perform prset analyses on the data
        baseTarget = combine_map(x: pheno, y: sumstat, by:0, column: ["name", "name2"], dup: "remove") \
            | combine(snp) \
            | combine(gtfInput) \
            | combine(genotype) \
            | map{ a -> def meta = a[0].clone()
                        meta["gtf"] = a[5]
                        meta["keepBest"] = "true"
                        meta["highRes"] = "false"
                        return([
                            meta,   // meta information
                            a[1],   // target phenotype
                            a[4],   // QCed SNPs
                            a[3],   // summary statistic
                            a[7],  // bed
                            a[8],  // bim
                            a[9],  // fam
                            a[6]   /*gtf*/ ])} 
        baseTarget \
            | combine(wind5) \
            | combine(wind3) \
            | combine(Channel.of(10000)) \
            | combine(filter_gmt.out) \
            | prset_analysis
        // 3. For each gene set that has competitive p-value < 0.05, 
        //    we will do high resolution scoring on them to do get 
        //    optimum signal
        prset_analysis.out.summary \
            | combine(filter_gmt.out) \
            | extract_significant_gmt
        baseTarget \
            | combine(extract_significant_gmt.out, by: 0) \
            | map{ a -> def meta = a[0].clone()
                        meta["oriName"] = meta["name"]
                        meta["name"] = meta["name"]+"-hybrid"
                        meta["highRes"] = "true"
                        return([
                            meta,   // meta information
                            a[1],   // target phenotype
                            a[2],   // QCed SNP
                            a[3],   // Sumstat
                            a[4],   // bed
                            a[5],   // bim
                            a[6],   // fam
                            a[7],   // gtf
                            "${params.wind5}",  // wind5
                            "${params.wind3}",  // wind3
                            0,      // perm
                            a[8]    // gmt
                        ])
                        } \
            | highResPRSet
        highRes = highResPRSet.out.summary \
            | combine(highResPRSet.out.best, by: 0) \
            | combine(highResPRSet.out.snp, by: 0) \
            | map{ a -> def meta = a[0].clone()
                        meta.remove("name")
                        meta["name"] = meta.remove("oriName")
                        meta.remove("keepBest")
                        meta.remove("highRes")
                        return([
                            meta,
                            a[1],   // summary
                            a[2],   // best
                            a[3]    /* SNPs */ ])} \
            | combine(Channel.of("PRSet-hybrid"))

        prsRes = prset_analysis.out.summary \
            | combine(prset_analysis.out.best, by: 0) \
            | combine(prset_analysis.out.snp, by: 0) \
            | map{ a -> removeMeta(x: a, keys: ["keepBest", "highRes"])} \
            | combine(Channel.of("PRSet")) \
            | mix(highRes) 
         // 4. Perform classificiation using PRS from the PRSet analyses
        combine_map(x: pheno, y: prsRes, by: 0, column: ["name", "name2", "typeAscertain", "fold", "typeAnalysis"]) \
            | supervised_pathway_classification
        // 5. Perform PRSice analysis using both high resolution scoring and fast score
        //    We did fast score so that we don't get like ~7000 scores from PRSice, which 
        //    can be extremely memory intensive
        singlePRSInput = combine_map(x: pheno, y: sumstat, by:0, column: ["name", "name2"], dup: "remove") \
            | combine(snp) \
            | combine(genotype) \
            | map{ a -> def meta = a[0].clone()
                        return([
                            meta,   // meta information
                            a[1],   // target phenotype
                            a[4],   // QCed SNPs
                            a[3],   // summary statistic
                            a[5],   // bed
                            a[6],   // bim
                            a[7]    /* fam */])} 
        // run PRSice
        singlePRSInput \
            | map{ a -> addMeta(x: a, meta: ["allScore": "false"])} \
            | prsice_analysis
        singlePRSInput \
            | map{ a -> addMeta(x: a, meta: ["allScore": "true"])} \
            | allScorePRSice
        allScore = allScorePRSice.out.all  \
            | map{ a -> removeMeta(x: a, keys: ["allScore"])}
        prsiceRes = prsice_analysis.out.best \
            | map{ a -> removeMeta(x: a, keys: ["allScore"])} \
            | combine(allScore, by: 0) \
            | combine(Channel.of("PRSice"))
        // 6. Also perform PRS analysis using lassosum. This provide a rough comparison
        //    of PRSet against the state of the art software (not going to do LDpred2 as
        //    that is way too time consuming
        // our container has lassosum installed. but that doesn't include our
        // custome script that ue
        singlePRSInput \
            | map{ a -> addMeta(x: a, meta: ["allScore": "true"])} \
            | map{ a -> addMeta(x: a, meta: ["best": "true"])} \
            | combine(lassosum) \
            | lassosum_analysis
        singlePRS = lassosum_analysis.out.best \
            | combine(lassosum_analysis.out.all, by: 0) \
            | map{ a -> removeMeta(x: a, keys: ["allScore", "best"])} \
            | combine(Channel.of("lassosum")) \
            | mix(prsiceRes)
        combine_map(x: pheno, y: singlePRS, by: 0, 
                column: ["name", "name2", "typeAscertain", "fold", "typeAnalysis"]) \
            | supervised_classification_with_single_prs        
        result = supervised_classification_with_single_prs.out \
            | mix(supervised_pathway_classification.out.res) \
            | collect
        combine_files("${params.out}.csv", "result", result)
        set_results = supervised_pathway_classification.out.set \
            | collect 
        combine_sets("${params.out}-sets.csv", "result", set_results)

            /*
        // 5. Perform PRSice analysis using both high resolution scoring and fast score
        //    We did fast score so that we don't get like ~7000 scores from PRSice, which 
        //    can be extremely memory intensive
        pheno \
            | combine(sumstat, by: 0) \
            | combine(Channel.of("TraitB")) \
            | combine(Channel.of("Type")) \
            | combine(Channel.of("Analysis")) \
            | map{ a -> [   a[0],   // Phenotype Name
                            a[5],   // stub
                            a[1],   // fold
                            a[6],   // type
                            a[7],   // analysis
                            a[2],   // training
                            a[3],   // validate
                            a[4]    ]} \
            | combine(genotype) \
            | combine(snp) \
            | combine(xregion) \
            | combine(prsice) \
            | (cross_trait_prsice_cv_analysis & cross_trait_fast_prsice_cv_analysis) 
        prsiceRes = cross_trait_prsice_cv_analysis.out \
            | combine(cross_trait_fast_prsice_cv_analysis.out, by: [0, 1, 2, 3, 4, 5])
        // 6. Also perform PRS analysis using lassosum. This provide a rough comparison
        //    of PRSet against the state of the art software (not going to do LDpred2 as
        //    that is way too time consuming
        pheno \
            | combine(sumstat, by: 0) \
            | combine(Channel.of("TraitB")) \
            | combine(Channel.of("Type")) \
            | combine(Channel.of("Analysis")) \
            | map{ a -> [   a[0],   // Phenotype Name
                            a[5],   // stub
                            a[1],   // fold
                            a[6],   // type
                            a[7],   // analysis
                            a[2],   // training
                            a[3],   // validate
                            a[4]    ]} \
            | combine(genotype) \
            | combine(snp) \
            | combine(xregion) \
            | combine(lassosum) \
            | cross_trait_lassosum_cv_analysis
        
        cross_trait_lassosum_cv_analysis.out \
            | mix(prsiceRes) \
            | cross_trait_single_prs_classification \
            | modify_classification_results
        results = prediction_with_pathway.out.res \
            | mix(modify_classification_results.out) \
            | collect 
        sipmle_result_combine("main", results)*/
}
