#BSUB -L /bin/sh
#BSUB -J SCZ
#BSUB -q premium
#BSUB -W 96:00
#BSUB -P acc_psychgen
#BSUB -o scz.o%J
#BSUB -eo scz.e%J
#BSUB -M 10000
#BSUB -n 1

module load singularity
module load java
project=/sc/arion/projects/psychgen/projects/prs/set_based_polygenic_score/
mount=${project}/tmp
export _JAVA_OPTIONS=-Djava.io.tmpdir=${mount}
data=${project}/analysis/preparation/data/

nextflow run \
    ${project}/script/2_real_data_analysis.nf \
    --loc ${data}/reference/Ensembl.regions \
    --gmt ${data}/gene_sets/ \
    --gtf ${data}/reference/Homo_sapiens.GRCh37.75.gtf.gz \
    --bfile ${project}/data/genotype/swedan.qc \
    --dropout ${project}/data/genotype/swedan-qc.dropout \
    --cov ${project}/data/phenotype/SCZ/swedan.use.cov \
    --fam ${project}/data/genotype/swedan-qc.fam \
    --snp ${project}/data/genotype/swedan.qc.snp \
    --json ${project}/script/JSON/scz.json \
    --gtex ${data}/gene_sets/celltype/ \
    --scores ${data}/reference/ldsc/ \
    --freq ${data}/reference/freq/chr@.frq \
    --expert ${project}/data/reference/Tissue_expert.csv \
    --malacard ${data}/gene_sets/gene_set_ranking \
    --gene ${project}/data/Malacard.csv \
    --wind5 35 \
    --wind3 10 \
    --perm 10000 \
    -resume \
    -with-singularity ${project}/container/prset_analyses_container.sif
