library(ggplot2)
library(data.table)
library(magrittr)
library(cowplot)
library(ellipse)
library(here)
top.quantile <- data.table(decile=0:9, enrichment=rnorm(10)) %>%
    rbind(., data.table(decile=10, enrichment=rnorm(1, mean=10))) %>%
    .[,Group:=ifelse(decile==10,"Top","Others")]

top.quantile.group <- top.quantile %>%
    .[,.(m=mean(enrichment)), by=Group]
top.plot <- top.quantile %>%
    ggplot(aes(x = decile, y = enrichment, color = Group)) +
    geom_point(size = 5) +
    theme_classic() +
    scale_color_npg() +
    geom_hline(data = top.quantile.group,
               aes(yintercept = m, color = Group),
               linetype = "dashed") +
    geom_segment(
        x = 10,
        xend = 10,
        y = top.quantile.group[Group == "Others", m],
        yend = top.quantile.group[Group != "Others", m],
        size=1, 
        arrow = arrow(length=unit(0.5,"cm")),
        color="black"
    )+
    geom_segment(
        x = 10,
        xend = 10,
        yend = top.quantile.group[Group == "Others", m],
        y = top.quantile.group[Group != "Others", m],
        size=1, 
        arrow = arrow(length=unit(0.5,"cm")),
        color="black"
    )+
    guides(color=FALSE)+
    theme(plot.title = element_text(size=16, face="bold"),
          axis.title = element_text(size=16, face="bold"),
          axis.text = element_blank(),
          axis.ticks = element_blank(),
          axis.line = element_line(arrow=arrow(length=unit(0.5,"cm"))))+
    labs(x="Gene expression specificity (in deciles)",
         y="Level of enrichment")


linear.increase <- data.table(decile=0:10) %>%
    .[,enrichment:=rnorm(.N,mean=decile), by=decile]

linear.plot <- linear.increase %>%
    ggplot(aes(x = decile, y = enrichment)) +
    geom_point(size = 5,color="#4DBBD5FF") +
    theme_classic() +
    scale_color_npg() +
    geom_segment(
        x = 0,
        xend = 10,
        yend = linear.increase[decile == 10, enrichment],
        y = linear.increase[decile == 0, enrichment],
        size=1, 
        arrow = arrow(length=unit(0.5,"cm")),
        color="black"
    )+
    theme(plot.title = element_text(size=16, face="bold"),
          axis.title = element_text(size=16, face="bold"),
          axis.text = element_blank(),
          axis.ticks = element_blank(),
          axis.line = element_line(arrow=arrow(length=unit(0.5,"cm"))))+
    labs(x="Gene expression specificity (in deciles)",
         y="Level of enrichment")
fig1 <- plot_grid(top.plot +
              ggtitle("Non-linear assumption"),
          linear.plot +
              ggtitle("Linear assumption", ),
          labels = "AUTO")

ggsave(here("plot", "figure1.png"), height=7, width=10)
ggsave(here("plot", "figure1.tiff"), height=7, width=10)
