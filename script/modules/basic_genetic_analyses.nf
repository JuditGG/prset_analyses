process filter_bed{
    label 'normal'
    input: 
        tuple   val(meta),
                path(snp),
                path(pheno),
                path(bed),
                path(bim), 
                path(fam)
    output:
        tuple   val(meta),
                path("${name}-qc.bed"),
                path("${name}-qc.bim"), 
                path("${name}-qc.fam")
    script:
    name = meta instanceof Map? meta.name : meta
    base=bed.baseName
    """
    plink \
        --bfile ${base} \
        --extract ${snp} \
        --keep ${pheno} \
        --out ${name}-qc \
        --make-bed
    """
}

process filter_summary_statistic{
    label 'normal'
    // This process will filter summary statistics based on user input.
    // If xregion is not null, the regions indicated by xregion will be
    // removed. 
    // ambiguous SNPs will be removed depending on rmAmbig, which is
    // assumed to be T or F
    input:
        tuple   val(meta),
                path(sumstat),
                path(snp),
                path(bed),
                path(bim),
                path(fam),
                val(xregion),
                val(rmAmbig)
    output:
        tuple   val(meta),
                path("${name}-overlap.snp"), emit: snp
        tuple   val(meta),
                path("${name}-overlap.sumstat.gz"), emit: sumstat
    script:
    name = meta instanceof Map? meta.name : meta
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    ambig <- function(x, y) {
        if (x == "A" & y == "T")
            return(T)
        if (x == "T" & y == "A")
            return(T)
        if (x == "C" & y == "G")
            return(T)
        if (x == "G" & y == "C")
            return(T)
        return(F)
    }
    bim <- fread("${bim}", header = F)
    snps <- fread("${snp}", header = F)
    # Filter out post QCed SNPs
    bim %<>% .[V2 %in% snps[, V1]]
    if(${rmAmbig}){
        bim %<>%
            .[, Ambig := lapply(.SD, function(x) {
                    ambig(V5, V6)
                }), .SDcols = c("V5", "V6"), by = "V2"] %>%
            .[Ambig == F] %>%
            .[, -c("Ambig")]
    }
    if("${xregion}" != "null"){
        xregion <- "${xregion}" %>%
            strsplit(., split = ",") %>%
            unlist
        for (region in xregion) {
            if (region != "") {
                breakdown <- strsplit(region, ":") %>%
                    unlist
                range <- breakdown %>%
                    tail(n = 1) %>%
                    strsplit(., "-") %>%
                    unlist %>%
                    as.numeric
                chr <- breakdown %>%
                    head(n = 1) %>%
                    gsub("chr", "", .) %>%
                    as.numeric
                # Filter based on bim, as bim always contain all required information
                bim <-
                    bim[!(V1 == chr & V4 >= range[1] & V4 <= range[2])]
            }
        }
    }
    sumstat <- fread("${sumstat}")
    overlap <- bim[V2 %in% sumstat[, SNP]]
    maxNum <- max(sumstat[,N]) %>%
        max(., 30) %>%
        `/`(., 10)
    sumstat[SNP %in% overlap[, V2] & N > maxNum] %>%
        fwrite(
            .,
            "${name}-overlap.sumstat.gz",
            quote = F,
            na = "NA",
            sep = "\\t"
        )
    write.table(overlap, "${name}-overlap.snp", quote=F, row.names=F, col.names=F)
    """

}
process standardize_summary_statistic{
    label 'normal'
    input:
        tuple   val(meta),
                path(sumstat),
                val(rs),
                val(a1),
                val(a2),
                val(stat),
                val(p),
                val(se),
                val(ncol),
                val(n),
                val(beta)
    output:
      // from this point onward, the sumstat file is standardized
        tuple   val(meta),
                path("${name}-std.sumstat.gz")
    script:
    name = meta instanceof Map ? meta.name : meta
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    sumstat <- fread("${sumstat}")
    rmDupCol <- function(x, y, input){
        if(x != y & y %in% colnames(input)){
            return(input[, -c(y), with=F])
        }else{
            return(input)
        }
    }
    sumstat <- rmDupCol("${rs}", "SNP", sumstat)
    sumstat <- rmDupCol("${a1}", "A1", sumstat)
    sumstat <- rmDupCol("${a2}", "A2", sumstat)
    sumstat <- rmDupCol("${stat}", "BETA", sumstat)
    sumstat <- rmDupCol("${p}", "P", sumstat)
    sumstat <- rmDupCol("${ncol}", "N", sumstat)
    setnames(sumstat, "${rs}", "SNP")
    setnames(sumstat, "${a1}", "A1")
    setnames(sumstat, "${a2}", "A2")
    setnames(sumstat, "${stat}", "BETA")
    setnames(sumstat, "${p}", "P")
    if ("${beta}" != "true") {
        sumstat[, BETA := log(BETA)]
    }
    if ("${ncol}" == "null") {
        sumstat[, N := ${n}]
    } else{
        setnames(sumstat, "${ncol}", "N")
    }
    fwrite(
        sumstat,
        "${name}-std.sumstat.gz",
        quote = F,
        na = "NA",
        sep = "\\t"
    )
    """

}

process perform_gwas{
    label 'normal'
    afterScript 'rm *qassoc'
    input: 
        tuple   val(meta),
                path(pheno),
                path(snp),
                path(bed),
                path(bim), 
                path(fam)
    output:
        tuple   val(meta),
                path("${name}.assoc.gz"),
                val("SNP"),
                val("A1"),
                val("A2"),
                val("BETA"),
                val("P"),
                val("SE"),
                val("NMISS"),
                val("null"),
                val("true")
    script:
    name = meta instanceof Map ? meta.name : meta
    phenoName = meta instanceof Map && meta.containsKey("phenoName") ? meta.phenoName : name
    base=bed.baseName
    """
    plink \
        --bfile ${base} \
        --extract ${snp} \
        --assoc \
        --pheno ${pheno} \
        --pheno-name ${phenoName} \
        --out ${name} \
        --autosome \
        --allow-no-sex
    awk 'NR==FNR{a[\$2]=\$5; b[\$2]=\$6} NR!=FNR && FNR==1{print \$0,"A1 A2"} NR != FNR && FNR!=1 {print \$0,a[\$2],b[\$2]}' ${bim} ${name}.qassoc | gzip > ${name}.assoc.gz
    """
}

process meta_analysis{
    label 'medium'
    input:
        tuple   val(meta),
                path(target),
                path(base)
    output:
        tuple   val(meta),
                path(base),
                path(target),
                path("${name}-meta1.assoc.gz")
    script:
    name = meta instanceof Map ? meta.name : meta
    """
    echo "
    SCHEME SAMPLESIZE
    MARKER SNP
    ALLELE A1 A2
    EFFECT BETA
    PVALUE P
    WEIGHT N
    PROCESS ${base}
    
    MARKER SNP
    ALLELE A1 A2
    EFFECT BETA
    PVALUE P
    WEIGHT N
    PROCESS ${target}

    OUTFILE ${name}-meta .assoc
    ANALYZE 
    " > metal_command
    metal < metal_command
    gzip ${name}-meta1.assoc
    """
}

process modify_metal{
    label 'medium'
    input:
        tuple   val(meta),
                path(base),
                path(target),
                path(metaSumstat)
    output:
        tuple   val(meta),
                path("${name}-meta.assoc.gz")
    script:
    name = meta instanceof Map ? meta.name : meta
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    sumstat <- fread("${metaSumstat}")
    sumstat[, N := 0]
    setnames(sumstat, "MarkerName", "SNP")
    setnames(sumstat, "Allele1", "A1")
    setnames(sumstat, "Allele2", "A2")
    setnames(sumstat, "P-value", "P")
    add_n <- function(file, input, name){
        dat <- fread(file) %>%
            setnames(., name, "N2")
        input <- merge(input, dat[,c("SNP", "N2")], by = "SNP") %>%
            .[, N := N + N2] %>%
            .[, -c("N2")]
    }
    # This should work because data.table are passed by reference (no copy)
    dat <- fread("${base}")
    if("NMISS" %in% colnames(dat)){
        setnames(dat, "NMISS", "N2")
    }else{
        setnames(dat, "N", "N2")
    }
        
    sumstat <- merge(sumstat, dat[,c("SNP", "N2")], by = "SNP") %>%
            .[, N := N + N2] %>%
            .[, -c("N2")]
    dat <- fread("${target}")
    if("NMISS" %in% colnames(dat)){
        setnames(dat, "NMISS", "N2")
    }else{
        setnames(dat, "N", "N2")
    }
    sumstat <- merge(sumstat, dat[,c("SNP", "N2")], by = "SNP") %>%
            .[, N := N + N2] %>%
            .[, -c("N2")]
    sumstat <- sumstat[,-c("Weight")]
    setnames(sumstat, "Zscore", "BETA")
    fwrite(
      sumstat,
      "${name}-meta.assoc.gz",
      sep = "\\t",
      na = "NA",
      quote = F
    )
    """
}

process filter_fam{
    label 'normal'
    input:
        tuple   path(bed),
                path(bim),
                path(fam),
                path(qcFam),
                path(dropout)
    output:
        path("valid.fam")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    drop <- fread("${dropout}", header = F)
    qc <- fread("${qcFam}", header = F)
    fread("${fam}", header = F) %>%
        .[V2 %in% qc[,V2]] %>%
        .[! V2 %in% drop[,V1]] %>%
        fwrite(., "valid.fam", sep = "\\t", quote = F, na = "NA")
    """
}