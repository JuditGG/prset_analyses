process get_overlap_snps{
    executor 'local'
    label 'tiny'
    // This process extract SNPs found in both the genotype and the QCed SNPs.
    // Reformat the output to match the required input format of MAGMA
    input: 
        tuple   val(meta),
                path(snp),  // contain overlapped SNPs between GWAS and bim
                path(bed), 
                path(bim), 
                path(fam)
    output:
        tuple   val(meta),
                path ("${name}-extracted")
    script: 
    name = meta instanceof Map? meta.name : meta
    """
        awk 'NR==FNR {a[\$2]=2} NR!=FNR && \$2 in a {print \$2,\$1,\$4}' ${snp} ${bim} > ${name}-extracted
    """
}

process perform_annotation{
    label 'tiny'
    input:
        tuple   val(meta),
                path(snp),
                path(geneLoc),
                val(wind5),
                val(wind3)
    output:
        tuple   val(meta),
                path("${name}.genes.annot")

    script:    
    name = meta instanceof Map? meta.name: meta
    """
    wind_com=""
    if [ ! -z "${wind5}" ]; then
        wind_com="window=${wind5},${wind3}"
        if [ ${wind5} -eq ${wind3} ] || [ -z "${wind3}" ]; then
            wind_com="window=${wind5}"
        fi
    fi
    magma \
        --annotate \${wind_com}\
        --snp-loc ${snp} \
        --gene-loc ${geneLoc} \
        --out ${name}
    """
}

process magma_genotype{
    // Run MAGMA on genotyped data. Require the annotation
    // file, genotype file and also the phenotype
    label 'long'
    input:
        tuple   val(meta),
                path(pheno),
                path(annot),
                path(bed), 
                path(bim), 
                path(fam)
    output:
        tuple   val(meta),
                path("${name}.genes.out"),
                path("${name}.genes.raw")
    script:
    base=bed.baseName
    name = meta instanceof Map ? meta.name : meta
    phenoName = meta instanceof Map && meta.containsKey("phenoName") ? meta.phenoName : name
    """
    magma \
        --bfile ${base} \
        --gene-annot ${annot} \
        --out ${name} \
        --pheno file=${pheno} use=${phenoName}
    """
}

process magma_sumstat{
    // Run MAGMA on the summary statistic file. 
    // The genotype file is also included for LD
    // calculation
    afterScript "rm ${name}.sumstat"
    label 'long'

    input:
        tuple   val(meta),
                path(ss),
                path(annot),
                path(bed),
                path(bim),
                path(fam)
    output:
        tuple   val(meta),
                path("${name}.sumstat.genes.out"), 
                path("${name}.sumstat.genes.raw")
    script:
    base=bed.baseName
    name = meta instanceof Map ? meta.name : meta
    """
    # MAGMA doesn't support GZ input. Therefore we gunzip
    # the summary statistic file if it is gzipped
    if gzip -t ${ss}; then
        zcat ${ss} > ${name}.sumstat
    else
        cat ${ss} > ${name}.sumstat
    fi
    magma \
        --bfile ${base} \
        --gene-annot ${annot}  \
        --pval ${name}.sumstat use=SNP,P ncol=N \
        --out ${name}.sumstat
    """
}

process magma_meta{
    // Run the MAGMA meta-analysis, combining results 
    // from the genotyped and summary stat MAGMA run
    label 'long'
    input:
        tuple   val(meta),
                path(ss_out), 
                path(ss_raw),
                path(raw_out),
                path(raw_raw)
    output:
        tuple   val(meta),
                path ("${name}.meta.genes.raw")
    script:
    name = meta instanceof Map ? meta.name : meta
    """
    magma \
        --meta raw=${raw_raw},${ss_raw}  \
        --out ${name}.meta
    """
}


process magma_gene_set{
    // Run the MAGMA set based analysis
    label 'long'
    input:
        tuple   val(meta),
                path(raw),
                path(gmt)
    output:
        tuple   val(meta),
                val("Set"),
                path("${name}.gsa.out")
    script:
    name = meta instanceof Map ? meta.name : meta
    """
    magma \
        --gene-results ${raw} \
        --set-annot ${gmt} \
        --out ${name}
    """
}

process magma_specificity{
    // Try to run MAGMA's celltype specificity method just for completeness. 
    // We don't look at this result in the paper, but it is something good to have 
    label 'long'
    input:
        tuple   path(spec),
                val(meta),
                path(raw)
    output:
        tuple   val(meta),
                val("Specificity"),
                path("${name}.${base}.celltype.gsa.out") 
    script:
    base=spec.baseName
    name = meta instanceof Map? meta.name: meta
    """
    magma \
        --gene-results ${raw} \
        --gene-covar ${spec} \
        --model direction-covar=greater \
        --out ${name}.${base}.celltype
    """
}

process modify_simulated_magma_output{
    label 'normal'
    input:
        tuple   val(meta),
                path(gsa)
    output:
        tuple   val(meta),
                path("*-magma.csv")
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    result <- fread(cmd = paste0("grep -v ^# ${gsa}")) %>%
        setnames(. , "FULL_NAME", "Set") %>%
        setnames(., "BETA", "Coefficient") %>%
        .[, c("Set", "Coefficient", "P")] %>%
        .[, Software := "MAGMA"] %>%
        .[, Perm := "${meta.name}"] %>%
        .[, NumSet := "${meta.numSet}"] %>%
        .[, Heritability := "${meta.herit}"] %>%
        .[, TargetSize := "${meta.targetSize}"] %>%
        .[, BaseSize := "${meta.baseSize}"] %>%
        fwrite(., "${meta.name}-${meta.numSet}-${meta.herit}-${meta.targetSize}-${meta.baseSize}-magma.csv")
    """
}
process modify_magma_output{
    label 'normal'
    input:
        tuple   val(meta),
                val(type),
                path("*")
    output:
        tuple   val(meta),
                val(type),
                path("${name}-${type}-magma.csv")
    script:
    name = meta instanceof Map? meta.name : meta
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    files <- list.files()
    result <- NULL
    for(i in files){
        tmp <- fread(cmd = paste0("grep -v ^# ",i)) %>%
            setnames(. , "FULL_NAME", "Set") %>%
            setnames(., "BETA", "Coefficient") %>%
            .[, c("Set", "Coefficient", "P")]
        result %<>% rbind(., tmp)
    }
    result %>%
        .[, Software := "MAGMA"] %>%
        .[, Type := "${type}"] %>%
        .[, Trait := "${name}"] %>%
        fwrite(., "${name}-${type}-magma.csv")
    """
}
