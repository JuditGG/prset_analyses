
process supervised_pathway_classification{
    label 'set_glmnet'
    input:
        tuple   val(meta),
                path(training),
                path(validate),
                path(summary),
                path(best),
                path(snps),
                val(software)
    output:
        path "${name}-${name2}-${ascertain}-${fold}-${analysis}-${gtf}-${software}.csv", emit: res optional true
        path "${name}-${name2}-${ascertain}-${fold}-${analysis}-${gtf}-${software}.set", emit: set optional true
    script:
    name = meta instanceof Map? meta.name: meta
    phenoName = meta instanceof Map && meta.containsKey("phenoName")? meta.phenoName : name
    name2 = meta instanceof Map && meta.containsKey("name2")? meta.name2 : name
    ascertain = meta instanceof Map && meta.containsKey("typeAscertain")? meta.typeAscertain : "All"
    analysis = meta instanceof Map && meta.containsKey("typeAnalysis")? meta.typeAnalysis : "CV"
    gtf = meta instanceof Map && meta.containsKey("gtf")? meta.gtf : "Normal"
    fold = meta instanceof Map && meta.containsKey("fold")? meta.fold : "1"
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(glmnet)
    library(data.table)
    library(doParallel)
    registerDoParallel(${task.cpus})

    performance <- fread("${summary}") %>%
      .[Set != "Base"]
    # Filter by Competitive P only when it is available
    if("Competitive.P" %in% colnames(performance)){
        performance %<>%
            .[, Competitive.P := as.numeric(Competitive.P)] %>%
            .[!is.na(Competitive.P) & Competitive.P < 0.05]
    }
    select <- c("FID", "IID", performance[, Set])
    if(length(select) == 2){
        # only got FID and IID
         res <- data.table(
                Sample = "Training",
                R2 = NA
            ) %>%
            rbind(
                .,
                data.table(
                Sample = "Validate",
                R2 = NA
                )
            ) %>%
            .[, Sample.Type := "${ascertain}"] %>%
            .[, Analysis.Type := "${analysis}"] %>%
            .[, GTF.Type := "${gtf}"] %>%
            .[, Fold := "${fold}"] %>%
            .[, TraitA := "${name}"] %>%
            .[, TraitB := "${name2}"] %>%
            .[, Software := "${software}"] %>%
            .[, Genome := NA ] %>%
            .[, Num.SNP := NA]
            colnames(res) <- gsub(".V1", "", colnames(res))
            fwrite(res, "${name}-${name2}-${ascertain}-${fold}-${analysis}-${gtf}-${software}.csv", na="NA", quote=F)
            quit()
    }
    prs <- fread("${best}", select = select)
    # Remove duplicated PRS. This is not required as in theory, glmnet should 
    # double weight the duplicates. However, this should help reduce the size 
    # of the matrix, therefore improve our performance
    dup.col <- !duplicated(as.list(prs))
    # Add back IID, which is usually duplicate of FID
    dup.col[2] <- T
    select <- colnames(prs)[dup.col]
    select <- select[-c(1:2)]
    prs <- prs[,dup.col, with=F]
    training.pheno <- fread("${training}") %>%
        .[, c("FID", "IID", "${phenoName}")] %>%
        .[!is.na(${phenoName})]
    valid.pheno <- fread("${validate}") %>%
        .[, c("FID", "IID", "${phenoName}")] %>%
        .[!is.na(${phenoName})]
    
    prs.target <- prs %>%
        merge(training.pheno[,c("FID", "IID", "${phenoName}")], .)
    snps <- fread("${snps}")
    num.genome <- sum(snps[,Base])
    included <- snps[,select, with=F] %>%
        apply(., 1, sum) 
    num.snps <- sum(included > 0)
    get.prs.r2 <- function(model, input, mat){
      pred <- predict(model, newx=mat, type="response", s="lambda.min")
      obs.r2 <- cor(pred, input[,${phenoName}])^2
      return(obs.r2)
    }
    x.matrix <- model.matrix(~., prs.target[, -c("FID", "IID", "${phenoName}")])[,-1]
    model <-
      cv.glmnet(y = as.matrix(prs.target[, c("${phenoName}")]), 
                x = x.matrix,
                parallel=T)
    coefs <- coef.glmnet(model, s="lambda.min")
    coef.res <-  data.frame(Set=row.names(coefs)[coefs[,1] != 0], Coef=coefs[coefs[,1] != 0]) %>%
        as.data.table %>%
        .[, Sample.Type := "${ascertain}"] %>%
        .[, Analysis.Type := "${analysis}"] %>%
        .[, GTF.Type := "${gtf}"] %>%
        .[, Fold := "${fold}"] %>%
        .[, TraitA := "${name}"] %>%
        .[, TraitB := "${name2}"] %>%
        .[, Software := "${software}"]
    fwrite(coef.res, "${name}-${name2}-${ascertain}-${fold}-${analysis}-${gtf}-${software}.set")
    prs.validate <- prs %>%
      merge(valid.pheno[,c("FID", "IID", "${phenoName}")], .)
    # Can get R2 by doing the following
    validate.matrix <- model.matrix(~., prs.validate[, -c("FID", "IID", "${phenoName}")])[,-1]
    training.r2 <- get.prs.r2(model, prs.target, x.matrix)
    validate.r2 <- get.prs.r2(model, prs.validate, validate.matrix)
    #validate.refit <- refit.r2(model, prs.validate, validate.matrix)
    res <- data.table(
      Sample = "Training",
      R2 = training.r2[1]
    ) %>%
      rbind(
        .,
        data.table(
          Sample = "Validate",
          R2 = validate.r2[1]
        )
      ) %>%
      .[, Sample.Type := "${ascertain}"] %>%
      .[, Analysis.Type := "${analysis}"] %>%
      .[, GTF.Type := "${gtf}"] %>%
      .[, Fold := "${fold}"] %>%
      .[, TraitA := "${name}"] %>%
      .[, TraitB := "${name2}"] %>%
      .[, Software := "${software}"] %>%
      .[, Genome := num.genome ] %>%
      .[, Num.SNP := num.snps]
    colnames(res) <- gsub(".V1", "", colnames(res))
    fwrite(res, "${name}-${name2}-${ascertain}-${fold}-${analysis}-${gtf}-${software}.csv")
    """
}

process unsupervised_classification_with_set{
    label 'set_glmnet'
    input:
        tuple   val(meta),
                path(training),
                path(validate),
                path(summary),
                path(best), 
                path(snps),
                val(software)
    output:
        path "${name}${addedName}-${fold}-${ascertain}-${analysis}-${software}-${gtf}.csv"
    script:
    name = meta instanceof Map ? meta.name: meta
    phenoName = meta instanceof Map && meta.containsKey("phenoName")? meta.phenoName : name
    ascertain = meta instanceof Map && meta.containsKey("typeAscertain")? meta.typeAscertain : "CV"
    fold = meta instanceof Map && meta.containsKey("fold")? meta.fold : "1"
    subtype = meta instanceof Map && meta.containsKey("Subtype")? meta.Subtype : phenoName
    addedName = meta instanceof Map && meta.containsKey("name2") ? "-${meta.name2}" : ""
    analysis = meta instanceof Map && meta.containsKey("typeAnalysis")? meta.typeAnalysis : "CV"
    gtf = meta instanceof Map && meta.containsKey("gtf")? meta.gtf : "Normal"
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    library(cluster)
    library(pdfCluster)
    library(glmnet)
    library(doParallel)
    registerDoParallel(${task.cpus})

    performance <- fread("${summary}") %>%
      .[Set != "Base"]
    # Filter by Competitive P only when it is available
    if("Competitive.P" %in% colnames(performance)){
        performance %<>%
            .[, Competitive.P := as.numeric(Competitive.P)] %>%
            .[!is.na(Competitive.P) & Competitive.P < 0.05]
    }
    select <- c("FID", "IID", performance[, Set])
    if(length(select) == 2){
        # only got FID and IID
         res <- data.table(
                Sample = "Training",
                R2 = NA
            ) %>%
            rbind(
                .,
                data.table(
                Sample = "Validate",
                R2 = NA
                )
            ) %>%
            .[, Sample.Type := "${ascertain}"] %>%
            .[, Analysis.Type = "${analysis}"] %>%
            .[, Fold := "${fold}"] %>%
            .[, GWAS.Trait := "${name}"] %>%
            .[, Software := "${software}"] %>%
            .[, Genome := NA ] %>%
            .[, GTF.Type := "${gtf}"] %>%
            .[, Num.SNP := NA]
            colnames(res) <- gsub(".V1", "", colnames(res))
            fwrite(res, "${name}${addedName}-${fold}-${ascertain}-${analysis}-${software}-${gtf}.csv", na="NA", quote=F)
            quit()
    }
    # Count number of SNPs
    snps <- fread("${snps}")
    num.genome <- sum(snps[,Base])
    included <- snps[,select[-c(1:2)], with=F] %>%
        apply(., 1, sum) 
    num.snps <- sum(included > 0)
    
    # Now use GLM Net to select Pathways from the data
    prs <- fread("${best}", select = select)
    # Remove duplicated PRS. This is not required as in theory, glmnet should 
    # double weight the duplicates. However, this should help reduce the size 
    # of the matrix, therefore improve our performance
    dup.col <- !duplicated(as.list(prs))
    # Add back IID, which is usually duplicate of FID
    dup.col[2] <- T
    select <- colnames(prs)[dup.col]
    select <- select[-c(1:2)]
    prs <- prs[,dup.col, with=F]
    scales <- colnames(prs[, -c("FID", "IID")])
    # Scale the PRS so that when we do clustering, sets with larger value will not
    # dominate the results
    prs[, (scales) := lapply(.SD, scale), .SDcols=scales]
    training.pheno <- fread("${training}") %>%
        .[, c("FID", "IID", "${phenoName}")] %>%
        .[!is.na(${phenoName})]
    valid.pheno <- fread("${validate}") %>%
        .[, c("FID", "IID", "${phenoName}", "${subtype}")] %>%
        .[!is.na(${phenoName})]
    
    prs.target <- merge(prs, training.pheno)
    x.matrix <- model.matrix(~., prs.target[, -c("FID", "IID", "${phenoName}")])[,-1]
    model <- cv.glmnet(y = as.matrix(prs.target[, c("${phenoName}")]), x = x.matrix,
                parallel=T)
    # Extract pathways with non-zero coefficients
    coefs <- coef(model, s='lambda.min')
    selected <- gsub("`","",rownames(coefs)[coefs[,1] != 0])
    # Remove the intercept, assuming the intercept is never 0
    selected  <- selected[-1]
    prs <- prs[,c("FID", "IID", selected), with = F]
    prs.validate <- merge(prs, valid.pheno) %>%
        .[ !is.na(${subtype})]

    get.glmnet.r2 <- function(model, input, mat){
      pred <- predict(model, newx=mat, type="response", s="lambda.min")
      obs.r2 <- cor(pred, input[,${phenoName}])^2
      return(obs.r2)
    }
    NagelkerkeR2 <- function (rr){
        n <- nrow(rr\$model)
        R2 <- (1 - exp((rr\$dev - rr\$null)/n))/(1 - exp(-rr\$null/n))
        return(R2)
    }
    dist.matrix <- dist(prs.validate[,selected, with=F])
    k.res <- NULL
    for(i in 1:30){
        km.multi <- kmeans( prs.validate[,selected, with=F],
                            centers=i,
                            algorithm="MacQueen",
                            iter.max=1000)
        multi.ss <- silhouette(km.multi\$cluster, dist.matrix)
        if(i == 1){
            multi.ss <- as.matrix(data.table(0,0,0))
        }
        k.res %<>% rbind(.,
            data.table( k=i,
                        ss=mean(multi.ss[,3])))
    }
    best.k <- k.res[which.max(ss), k]
    cluster <- kmeans(prs.validate[, selected, with=F], 
                    centers=best.k,
                    algorithm = "MacQueen",
                    iter.max=1000)
    prs.validate[, Group := cluster\$cluster]
    prediction <- glm(${subtype}~Group, prs.validate, family=binomial)
    r2 <- NagelkerkeR2(prediction)
    prediction.k2 <- prediction
    r2.k2 <- r2
    if(best.k != 2){
        cluster <- kmeans(prs.validate[, selected, with=F], 
                        centers=2,
                        algorithm = "MacQueen",
                        iter.max=1000)
        prs.validate[, Group := cluster\$cluster]
        prediction.k2 <- glm(${subtype}~Group, prs.validate, family=binomial)
        r2.k2 <- NagelkerkeR2(prediction.k2)
    }

    res <- data.table( Fold = ${fold}, 
                TraitA = "${name}",
                Sample.Type = "${ascertain}",
                Software = "${software}",
                Analysis.Type = "${analysis}",
                GTF.Type = "${gtf}",
                Best.K = best.k, 
                Unsupervised.R2 = r2,
                Coefficient = coef(summary(prediction))["Group", "Estimate"],
                SE = coef(summary(prediction))["Group", "Std. Error"],
                P = coef(summary(prediction))["Group", "Pr(>|z|)"],
                K2.R2 = r2.k2,
                K2.Coefficient = coef(summary(prediction.k2))["Group", "Estimate"],
                K2.SE = coef(summary(prediction.k2))["Group", "Std. Error"],
                K2.P = coef(summary(prediction.k2))["Group", "Pr(>|z|)"])
    if( "${addedName}" != ""){
        res[, TraitB := "${meta.name2}"]
    }
    fwrite(res, "${name}${addedName}-${fold}-${ascertain}-${analysis}-${software}-${gtf}.csv")
    
    """
}



process supervised_classification_with_single_prs{
    label 'set_glmnet'
    input:
        tuple   val(meta),
                path(training),
                path(validate),
                path(best),
                path(allscore),
                val(software)
    output:
        path "${name}-${name2}-${ascertain}-${fold}-${analysis}-${software}.csv"
    script:
    name = meta instanceof Map? meta.name: meta
    phenoName = meta instanceof Map && meta.containsKey("phenoName")? meta.phenoName : name
    name2 = meta instanceof Map && meta.containsKey("name2")? meta.name2 : name
    ascertain = meta instanceof Map && meta.containsKey("typeAscertain")? meta.typeAscertain : "All"
    analysis = meta instanceof Map && meta.containsKey("typeAnalysis")? meta.typeAnalysis : "CV"
    fold = meta instanceof Map && meta.containsKey("fold")? meta.fold : "1"
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    library(glmnet)
    library(doParallel)
    registerDoParallel(${task.cpus})
    # Mainly aggregate result and calculate MSE
    prs <- fread("${best}", select = c("FID", "IID", "PRS"))
    training.pheno <- fread("${training}") %>%
        .[, c("FID", "IID", "${phenoName}")] %>%
        .[!is.na(${phenoName})]
    valid.pheno <- fread("${validate}") %>%
        .[, c("FID", "IID", "${phenoName}")] %>%
        .[!is.na(${phenoName})]
    prs.training <- merge(training.pheno, prs)
    prs.validate <- merge(valid.pheno, prs)
    get.prs.r2 <- function(x, obs.model) {
        obs.r2 <- predict(obs.model, newdata=x, type="response") %>%
            cor(., x[,${phenoName}])
        obs.r2 <- obs.r2^2
        return(obs.r2)
    }
    obs.model <- glm(${phenoName}~PRS, data = prs.training) 
    training.r2 <- get.prs.r2(prs.training, obs.model)
    validate.r2 <- get.prs.r2(prs.validate, obs.model)

    # Then we do the same analysis but with all scores
    # Format is always FID IID PRS1 PRS2 etc
    all <- fread("${allscore}")
    # This is for lassosum as there can be multiple column with 0
    dup.col <- !duplicated(as.list(all))
    dup.col[2] <- T
    all <- all[,dup.col, with=F]
    colnames(all) <- c("FID", "IID", paste("V",1:(ncol(all)-2), sep=""))
    prs.training <- merge(training.pheno, all)
    prs.validate <- merge(valid.pheno, all)
    # Now do glm model
    get.glmnet.r2 <- function(model, input, mat){
      pred <- predict(model, newx=mat, type="response", s="lambda.min")
      obs.r2 <- cor(pred, input[,${phenoName}])^2
      return(obs.r2)
    }
    x.matrix <- model.matrix(~., prs.training[, -c("FID", "IID", "${phenoName}")])[,-1]
    model <-
      cv.glmnet(y = as.matrix(prs.training[, c("${phenoName}")]), 
                x = x.matrix,
                parallel=T)
    validate.matrix <- model.matrix(~., prs.validate[, -c("FID", "IID", "${phenoName}")])[,-1]
    training.glmnet.r2 <- get.glmnet.r2(model, prs.training, x.matrix)
    validate.glmnet.r2 <- get.glmnet.r2(model, prs.validate, validate.matrix)
    res <- data.table(Sample = "Training",
               R2 = training.r2[1],
               Software = "${software}") %>%
        rbind(.,
              data.table(Sample = "Validate",
                         R2 = validate.r2[1],
               Software = "${software}")) %>%
        rbind(.,
              data.table(Sample = "Training",
                         R2 = training.glmnet.r2[1],
               Software = "${software}GLM")) %>%
        rbind(.,
              data.table(Sample = "Validate",
                         R2 = validate.glmnet.r2[1],
               Software = "${software}GLM")) %>%
        .[, Sample.Type := "${ascertain}"] %>%
        .[, Fold := "${fold}"] %>%
        .[, Analysis.Type := "${analysis}"] %>%
        .[, GTF.Type := NA] %>%
        .[, TraitA := "${name}"] %>%
        .[, TraitB := "${name2}"] 
    colnames(res) <- gsub(".V1", "", colnames(res))
    fwrite(res, "${name}-${name2}-${ascertain}-${fold}-${analysis}-${software}.csv")
    """
}



process unsupervised_classification_with_single_prs{
    label 'set_glmnet'
    input:
        tuple   val(meta),
                path(training),
                path(validate),
                path(best),
                path(allscore),
                val(software)
    output:
        path "${name}${addedName}-${fold}-${ascertain}-${analysis}-${software}-single.csv"
    script:
    name = meta instanceof Map? meta.name: meta
    phenoName = meta instanceof Map && meta.containsKey("phenoName")? meta.phenoName : name
    ascertain = meta instanceof Map && meta.containsKey("typeAscertain")? meta.typeAscertain : "All"
    fold = meta instanceof Map && meta.containsKey("fold")? meta.fold : "1"
    subtype = meta instanceof Map && meta.containsKey("Subtype")? meta.Subtype : phenoName
    addedName = meta instanceof Map && meta.containsKey("name2") ? "-${meta.name2}" : ""
    analysis = meta instanceof Map && meta.containsKey("typeAnalysis")? meta.typeAnalysis : "CV"
    
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    library(cluster)
    library(pdfCluster)
    library(glmnet)
    library(doParallel)
    registerDoParallel(${task.cpus})
    best <- fread("${best}") %>%
        .[, PRS := lapply(.SD, scale), .SDcols=c("PRS")]
    if("In_Regression" %in% colnames(best)){
        best %<>% .[, -c("In_Regression")]
    }
    training.pheno <- fread("${training}") %>%
        .[, c("FID", "IID", "${phenoName}")] %>%
        .[!is.na(${phenoName})]
    valid.pheno <- fread("${validate}") %>%
        .[, c("FID", "IID", "${phenoName}", "${subtype}")] %>%
        .[!is.na(${phenoName})]
    training.prs <- merge(training.pheno, best)
    get.prs.r2 <- function(x, obs.model) {
        obs.r2 <- predict(obs.model, newdata=x, type="response") %>%
            cor(., x[,${phenoName}])
        obs.r2 <- obs.r2^2
        return(obs.r2)
    }
    obs.model <- glm(${phenoName}~PRS, data = training.prs) 
    training.r2 <- get.prs.r2(training.prs, obs.model)
    NagelkerkeR2 <- function (rr){
        n <- nrow(rr\$model)
        R2 <- (1 - exp((rr\$dev - rr\$null)/n))/(1 - exp(-rr\$null/n))
        return(R2)
    }
    validate.prs <- merge(valid.pheno, best) %>%
        .[!is.na(${subtype})]
    dist.matrix <- dist(validate.prs[,"PRS", with=F])
    k.res <- NULL
    for(i in 1:min(30, nrow(unique(validate.prs[,"PRS", with=F])))){
        km.multi <- kmeans( validate.prs[,"PRS", with=F],
                            centers=i,
                            algorithm="MacQueen",
                            iter.max=1000)
        multi.ss <- silhouette(km.multi\$cluster, dist.matrix)
        if(i == 1){
            multi.ss <- as.matrix(data.table(0,0,0))
        }
        k.res %<>% rbind(.,
            data.table( k=i,
                        ss=mean(multi.ss[,3])))
    }
    best.k <- k.res[which.max(ss), k]
    cluster <- kmeans(validate.prs[, "PRS", with=F], 
                    centers=best.k,
                    algorithm = "MacQueen",
                    iter.max=1000)
    validate.prs[, Group := cluster\$cluster]
    prediction <- glm(${subtype}~Group, validate.prs, family=binomial)
    r2 <- NagelkerkeR2(prediction)
    prediction.k2 <- prediction
    r2.k2 <- r2
    if(best.k != 2){
        cluster <- kmeans(validate.prs[, "PRS", with=F], 
                        centers=2,
                        algorithm = "MacQueen",
                        iter.max=1000)
        validate.prs[, Group := cluster\$cluster]
        prediction.k2 <- glm(${subtype}~Group, validate.prs, family=binomial)
        r2.k2 <- NagelkerkeR2(prediction.k2)
    }
    res <- data.table( Fold = ${fold}, 
                TraitA = "${name}",
                Sample.Type = "${ascertain}",
                Software = "${software}",
                Analysis.Type = "${analysis}",
                Best.K = best.k, 
                Unsupervised.R2 = r2,
                Coefficient = coef(summary(prediction))["Group", "Estimate"],
                SE = coef(summary(prediction))["Group", "Std. Error"],
                P = coef(summary(prediction))["Group", "Pr(>|z|)"],
                K2.R2 = r2.k2,
                K2.Coefficient = coef(summary(prediction.k2))["Group", "Estimate"],
                K2.SE = coef(summary(prediction.k2))["Group", "Std. Error"],
                K2.P = coef(summary(prediction.k2))["Group", "Pr(>|z|)"])
    if("${addedName}" != ""){
        res[, TraitB := "${meta.name2}"]
    }
    fwrite(res, "${name}${addedName}-${fold}-${ascertain}-${analysis}-${software}-single.csv")
    
    """
}
