library(data.table)
library(magrittr)
args <- commandArgs(trailingOnly = TRUE)

pheno <- fread(args[1])
qc <- fread(args[2])
dropout <- fread(args[3], header = F)
cov <- fread(args[4])
out <- args[5]

get_age <- function(x, y) {
    max(x, y, na.rm = T)
}
model <- paste("PC", 1:15, collapse = "+", sep = "") %>%
    paste0("Paternal~Sex+Mum_age+Dad_age+Centre+Batch+", .) %>%
    as.formula
# Output samples used for subtyping
pheno %>%
    .[IID %in% qc[, V2]] %>%
    .[!IID %in% dropout[, V1]] %>%
    .[AD != 0] %>%
    fwrite(.,
           paste0(out, ".sub"),
           sep = "\t",
           na = "NA",
           quote = F)

pheno %>%
    .[IID %in% qc[, V2]] %>%
    .[!IID %in% dropout[, V1]] %>%
    .[Adopted == 0] %>% # Only allow non-addopted samples
    .[AD == 0] %>% # Only include non-AD patients for cleaner proxy data
    .[Mum_age <= 0, Mum_age := NA] %>% # Clean the age info
    .[Mum_death <= 0, Mum_death := NA] %>% # Clean the age info
    .[Dad_age <= 0, Dad_age := NA] %>% # Clean the age info
    .[Dad_death <= 0, Dad_death := NA] %>% # Clean the age info
    .[is.na(Mum_age) |
          (!is.na(Mum_age) &
               Mum_age > 60)] %>% # Only include parents that are old enough or didn't die too early
    .[is.na(Dad_age) | (!is.na(Dad_age) & Dad_age > 60)] %>%
    .[is.na(Dad_death) | (!is.na(Dad_death) & Dad_death > 60)] %>%
    .[is.na(Mum_death) | (!is.na(Mum_death) & Mum_death > 60)] %>%
    .[!(is.na(Mum_age) &
            is.na(Mum_death))] %>% # Only include samples with maternal age info
    .[!(is.na(Dad_age) &
            is.na(Dad_death))] %>% # Only include samples with paternal age info
    .[(!is.na(Mum_age) &
           !is.na(Mum_death)), Sanity_Mum := Mum_death >= Mum_age] %>% # Age sanity check for mum
    .[(!is.na(Dad_age) &
           !is.na(Dad_death)), Sanity_Dad :=  Dad_death >= Dad_age] %>% # Age sanity check for dad
    .[is.na(Sanity_Dad) | Sanity_Dad == TRUE] %>%
    .[is.na(Sanity_Mum) | Sanity_Mum == TRUE] %>%
    .[, -c("Sanity_Dad", "Sanity_Mum")] %>%
    .[Mum != -1] %>% # Remove samplew ith uncertain data e.g. prefer not to answer / don't know
    .[Dad != -1] %>%
    .[, c("Mum_age", "Dad_age") := list(get_age(Mum_age, Mum_death),
                                        get_age(Dad_age, Dad_death)),
      by = FID] %>%
    .[Dad <= 0, Dad := 0] %>% # Don't differentiate super control vs normal control
    .[Mum <= 0, Mum := 0] %>%
    .[, Paternal := Dad + Mum] %>%
    .[, Centre := as.factor(Centre)] %>%
    merge(., cov, by = c("FID", "IID")) %>%
    .[, .(FID = FID,
          IID = IID,
          Pheno = lm(model, data = .) %>%
              resid)] %>%
    setnames(., "Pheno", "AD")  %>%
    fwrite(.,
           out,
           sep = "\t",
           na = "NA",
           quote = F)
