library(cluster)
library(pdfCluster)
library(glmnet)
library(magrittr)
library(data.table)

best<- fread("AD-high-res.best.gz")
summary <- fread("AD-high-res.summary")
remove <- summary[Num_SNP < 10, Set]
best <- best[,-c(remove), with=F]
dup <-  !duplicated(as.list(best))
dup[2] <- T # Set IID as True
pheno <- fread("AD-adj")
scales  <- colnames(best[, -c("FID", "IID", "In_Regression", "Base")])
analy[, (cols) := lapply(.SD, scale), .SDcols=cols]
ad <- merge(pheno, best[,dup, with=F][,-c("In_Regression", "Base")])
x.matrix <- model.matrix(~., ad[, -c("FID", "IID", "AD")])[,-1]
model <- cv.glmnet(y = as.matrix(ad[, c("AD")]), x = x.matrix)
coefs <- coef(model, s='lambda.min')
selected <- gsub("`","",rownames(coefs)[coefs[,1] != 0])
selected  <- selected[-1]
sub <- fread("AD-adj.sub")
ad.sub <- merge(sub, best[,dup, with=F][,c("FID", "IID", selected), with=F])
dist.matrix <- dist(ad.sub[,selected, with=F])
k.res <- NULL
for(i in 1:30){
    km.multi <- kmeans( ad.sub[,selected, with=F],
                        centers=i,
                        algorithm="MacQueen",
                        iter.max=1000)
    multi.ss <- silhouette(km.multi$cluster, dist.matrix)
    if(i == 1){
        multi.ss <- as.matrix(data.table(0,0,0))
    }
    k.res %<>% rbind(.,
        data.table( k=i,
                    ss=mean(multi.ss[,3])))
}
best.k <- k.res[which.max(ss), k]
cluster <- kmeans(ad.sub[, selected, with=F], 
                    centers=best.k,
                    algorithm = "MacQueen",
                    iter.max=1000)
ad.sub[, Group := cluster$cluster]
apoe <- fread("/sc/arion/projects/psychgen2/ukb/beatrice/projects/AD/APOE_genotype/UKB_allSample_APOE_genotype_EUR_de.txt")[,c("IID", "e4_copy","e2_copy")]
ad.res <- merge(ad.sub[,c("IID", "Group", "Age")], apoe, by="IID")
