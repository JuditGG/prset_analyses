# Purpose 

This section gives details on the Nextflow script used to run simulation analyses to evaluate pathway enrichment of genetic signal by PRS vs MAGMA and LDSC. Pathwasy are defined using the six publicly available databaes (Biocarta, Pathway Interaction Database, Reactome, Mouse genome Database, KEGG and GO)

# Required software

1. [Nextflow](https://nextflow.io/)
    - This is required to run the pipeline
    - Only depends on java version 8 or later

2. [Singularity](https://singularity-tutorial.github.io/01-installation/)
    - This is required so that we can used the generated container

3. [LDSC](https://github.com/bulik/ldsc)

4. [MAGMA](https://ctg.cncr.nl/software/magma)
    
5. [PRSet](https://www.prsice.info/)

# Required input files

--loc --> Gene location file required by MAGMA. The gene location file must contain at least four columns, in this order: gene ID, chromosome, start site, stop site. Gene locations for protein-coding genes can be obtained from the NCBI site.

--gtf --> GTF file for PRSet. For example the [Homo_sapiens.GRCh37.75.gtf](ftp://ftp.ensembl.org/pub/release-75/​gtf/homo_sapiens/Homo_sapiens.GRCh37.75.gtf.gz) file.

--gmt --> GMT files. The GMT file format is a tab delimited file format that describes gene sets. These have been downloaded in the [1_prepare_workspace.nf](https://gitlab.com/choishingwan/prset_analyses/-/blob/master/script/1_prepare_workspace.nf) script.

--bfile --> Genotype file prefix. For example if using UK Biobank, the format is usually ukbXXXXX (where XXXXX is application number).

--fam --> FAM file after genetic quality control.

--snp --> SNP file after genetic quality control.

--cov --> File with covariates to be adjusted for. 

--dropout --> List with participant IDs that decided to dropout or withdrawed consent. These individuals are removed from the analysis.

--scores --> Path to folder with precalculated ld scores. Calculated in the [1_prepare_workspace.nf](https://gitlab.com/choishingwan/prset_analyses/-/blob/master/script/1_prepare_workspace.nf) script

--freq --> Minor allele frequency information required by LDSC. These have been calculated in the [1_prepare_workspace.nf](https://gitlab.com/choishingwan/prset_analyses/-/blob/master/script/1_prepare_workspace.nf) script. If the same file structure is followed, they are located in /data/reference/freq/

# Example script

The following is an example script used for running the simulations using genotype data from UK Biobank. 

``` bash

project=<folder containing all required data>
geno=<folder containing genotype information from UK biobank>

nextflow run \
    ${project}/script/3_simulation_analysis.nf \
    -resume \
    -with-singularity ${project}prset_analyses_container_latest.sif \
    --loc ${project}/data/reference/Ensembl.regions \
    --gtf ${project}/data/reference/Homo_sapiens.GRCh37.75.gtf.gz \
    --gmt ${project}/data/gene_sets \
    --bfile ${geno}/ukbXXXXX \
    --fam ${geno}ukbXXXXX-qc.fam \
    --snp ${geno}ukbXXXXX-qc.snplist \
    --cov ukbXXXXX.covar \
    --dropout wXXXXX_20210201.csv \
    --scores ${project}/data/reference/ldsc \
    --freq ${project}/data/reference/freq/chr@.frq \
    --wind3 35 \
    --wind5 10 \
    --perm 1000 \
    --simperm 20 \
    --out simulation \
    --seed 1234 \
    --xregion chr6:25000000-34000000 \
    --start 0

``` 
