# Real data analysis script

## Purpose 

This section gives details on the Nextflow scripts used to compare pathway enrichment performance of LDSC, MAGMA and PRSet using UK Biobank data and publicly available GWAS across six traits: low-density lipoproteins (LDL), CAD, schizophrenia, BMI, Alzheimer’s disease (proxy status) and alcohol consumption. 

## Required software

1. [Nextflow](https://nextflow.io/)
    - This is required to run the pipeline
    - Only depends on java version 8 or later

2. [Singularity](https://singularity-tutorial.github.io/01-installation/)
    - This is required so that we can used the generated container

3. [PLINK](https://www.cog-genomics.org/plink2)
    - This is required to perform GWAS on the target sample before running LDSC

4. [METAL](https://genome.sph.umich.edu/wiki/METAL)
    - This is required to run a meta-analysis between base and target sample before running LDSC

5. [LDSC](https://github.com/bulik/ldsc)

6. [MAGMA](https://ctg.cncr.nl/software/magma)
    
7. [PRSet](https://www.prsice.info/)


## Required input files

--loc --> Gene location file required by MAGMA. The gene location file must contain at least four columns, in this order: gene ID, chromosome, start site, stop site. Gene locations for protein-coding genes can be obtained from the NCBI site.

--gtf --> GTF file for PRSet. For example the [Homo_sapiens.GRCh37.75.gtf](ftp://ftp.ensembl.org/pub/release-75/​gtf/homo_sapiens/Homo_sapiens.GRCh37.75.gtf.gz) file.

--gmt --> GMT files. The GMT file format is a tab delimited file format that describes gene sets. These have been downloaded in the [1_prepare_workspace.nf](https://gitlab.com/choishingwan/prset_analyses/-/blob/master/script/1_prepare_workspace.nf) script.

--bfile --> Genotype file prefix. For example if using UK Biobank, the format is usually ukbXXXXX (where XXXXX is application number).

--fam --> FAM file after genetic quality control.

--snp --> SNP file after genetic quality control.

--cov --> File with covariates to be adjusted for. 

--json --> JSON file with information for each phenotype under study. You can find examples of JSON files strucutre used [here](https://gitlab.com/choishingwan/prset_analyses/-/tree/master/script/JSON).

--dropout --> List with participant IDs that decided to dropout or withdrawed consent. These individuals are removed from the analysis.

--sql --> UK Biobank SQL data base with phenotype information

--gtex --> Path to folder with tissue and cell type specificity files. These have been calculated in the [1_prepare_workspace.nf](https://gitlab.com/choishingwan/prset_analyses/-/blob/master/script/1_prepare_workspace.nf) script.

--scores --> Path to folder with precalculated ld scores. Calculated in the [1_prepare_workspace.nf](https://gitlab.com/choishingwan/prset_analyses/-/blob/master/script/1_prepare_workspace.nf) script

--freq --> Minor allele frequency information required by LDSC. These have been calculated in the [1_prepare_workspace.nf](https://gitlab.com/choishingwan/prset_analyses/-/blob/master/script/1_prepare_workspace.nf) script. If the same file structure is followed, they are located in /data/reference/freq/

--malacard --> Pathway level MalaCards scores. These have been calculated in the [1_prepare_workspace.nf](https://gitlab.com/choishingwan/prset_analyses/-/blob/master/script/1_prepare_workspace.nf) script.

--gene --> Malacard score for individual genes. This is used in this script to calculate null MalaCards relevance score for each pathway. A copy of the scores used can be found [in the data folder](https://gitlab.com/choishingwan/prset_analyses/-/blob/master/data)

--expert --> CSV containing expert opion. Provided [in data/reference folder](https://gitlab.com/choishingwan/prset_analyses/-/blob/master/data/reference).


## Data structure after running 1_prepare_workspace.nf

```
project
    |- script
    |   |- 2_real_data_analysis.nf
    |   |- modules
    |
    |- container
    |   |- prset_analyses_container_latest.sif
    |   
    |- data
        |- Malacard.csv (MalaCards information)
        |
        |- Tissue_expert (Opion about tissue relevance for each disease)
        |
        |- gene_sets
        |   |- MSigDB GMT files
        |   |
        |   |- gene_set_count (Number of genes with MalaCards scores for each gene set for each phenotype)
        |   |- gene_set_ranking (Gene set Malacards score for each phenotype)
        |   |
        |   |- dict (data dictionaries for GO and MGI)
        |   |
        |   |- celltype
        |       |- GTEx.gmt
        |       |- GTEx.magma
        |       |- skene.gmt
        |       |- skene.magma
        |   
        |- reference
            |- Ensembl.regions (Gene location file required by MAGMA. Input for parameter --loc)
            |- Homo_sapiens.GRCh37.75.gtf.gz
            |- freq (folder with the allele frequency information)
            |- ldsc (folder with LD scores)

Other required data for the analysis: 

        |- genotyped data
        |
        |- ukb-qc.snplist
        |- ukb-qc.fam
        |- ukb.cov
        |
        |- ukb.db (SQL database with UK Biobank phenotypes)
        |- JSON files with phenotype information

```

## Example script

The following is an example script used for running the analysis with the traits extracted from the UK Biobank.

``` bash
project=<folder containing all required data>
geno=<folder containing genotype information from UK biobank>

nextflow run \
    ${project}/script/prset_analyses-master/script/2_real_data_analysis.nf \
    -resume \
	-with-singularity ${project}prset_analyses_container_latest.sif \
    --loc ${project}/data/reference/Ensembl.regions \
    --gtf ${project}/data/reference/Homo_sapiens.GRCh37.75.gtf.gz \
    --gmt ${project}/data/gene_sets/ \
    --bfile ${geno}ukbXXXXX \
    --fam ${geno}ukbXXXXX-qc.fam \
    --snp ${geno}ukbXXXXX-qc.snplist \
    --json ${project}/<filename>.json \
    --cov /sc/arion/projects/data-ark/ukb/application/ukb18177/phenotype/ukbXXXXX.covar \
    --dropout /sc/arion/projects/data-ark/ukb/application/ukb18177/withdrawn/wXXXXX_20210201.csv \
    --sql ${app}/phenotype/ukbXXXXX.db \
    --gtex ${project}/data/gene_sets/celltype/ \
    --scores ${project}/data/reference/ldsc \
    --freq ${project}/data/reference/freq/chr@.frq \
    --malacard ${project}/data/gene_sets/gene_set_ranking \
    --gene ${project}/data/Malacard.csv \
    --expert ${project}/data/Tissue_expert.csv \
    --wind3 35 \
    --wind5 10 \
    --perm 10000 
```


